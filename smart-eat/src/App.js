import React, { Component } from 'react';
import './App.css';
import Header from './modules/Header/header.js';
import SmartEatPic from './kjoleskapdude.png';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header isFrontPageOpen={true} />
        <div className="container">
          <div className="row">
        	 <div className="col-6">
              <h1>Welcome to SmartEat</h1>
              <p>SmartEat is your new personal assistant when it comes to type one diabetes. SmartEat can monitor your blood sugar levels and 
              will automatically suggest what food you should or should not eat. It will also notify health personell and your next of kin if
              SmartEat sees that your condition becomes critical.</p> 
           </div>
           <div className="col-6">
             <img src={SmartEatPic} alt="Logo" />
           </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
