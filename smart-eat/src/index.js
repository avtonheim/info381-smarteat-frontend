import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import App from './App';
import CategoryPage from './modules/categoryPage';
import InputValues from './modules/inputValues';
import UserPage from './modules/userPage';
import FoodPage from './modules/foodPage';
import './index.css';

ReactDOM.render(
  <Router history={history}>
  	<div>
  		<Route exact={true} path='/' component={App}></Route>
  		<Route exact={true} path='/user/:userID' component={UserPage}></Route>
	    <Route exact={true} path='/input' component={InputValues}></Route>
  		<Route exact={true} path='/category/:catName' component={CategoryPage}></Route>
      <Route exact={true} path='/foodItem/:foodId' component={FoodPage}></Route>

  	</div>
  </Router>,
  document.getElementById('root')
);
