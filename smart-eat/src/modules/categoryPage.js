import React, { Component } from 'react';
import Header from './Header/header';

class CategoryPage extends Component {
	
	constructor(data) {
  		super();
  		this.state = {categoryName: data.match.params.catName};
  	}

  	setCategoryName(categoryName) {
  		this.setState({categoryName});
  	}

  	render() {
  	    return (
  	    	<div className="App">
  	    		<Header />
  	    		<div className="FoodContainer">
			        <h1>{this.state.categoryName}</h1>
		      </div>
  	    	</div>
    );
  }
}

export default CategoryPage;