import React, { Component } from 'react';
import ModalView from './modalView';

class InputValues extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: null,
      bloodSugar: 0,
      age: props.age,
      modalOpen: false,
      modalMessage: '',
      modalBody: '',
      fetchCategories: props.fetchCategories,
      updateBloodSugar: props.updateBloodSugar,
      bloodSugarChanged: props.bloodSugarChanged,
      fetchFood: props.fetchFood
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  changeBloodSugar(e) {
    const bloodSugar = parseInt(e.target.value, 0);
    this.state.updateBloodSugar(bloodSugar);
    this.state.bloodSugarChanged(bloodSugar);
    this.setState({
      bloodSugar: bloodSugar,
      modalOpen: true
    })
  }

  checkBloodSugar(bloodSugar, e) {

    if(bloodSugar <= 2 || bloodSugar >= 22)  {// Critical blood sugar levels, should call ambulance and guardian
      this.setState({
        modalOpen: true,
        modalMessage: "Critical status",
        modalBody: "I have called for help. Ambulance and your guardians has been notified. Please remain calm. If you just found this person, please stay with them until medical help has arrived.",
        modalStyleClass: 'alert'
      })

    } else if (bloodSugar <= 3 || bloodSugar >= 16) {
      // Contact parents and guardians
      this.setState({
        modalOpen: true,
        modalMessage: "Alerting guardians",
        modalBody: "I have alerted your guardians. Please remain calm.",
        modalStyleClass: 'relax'
      })
    } else {
      // Show question about how you are feeling with some nice emojies to press
      // Act accordingly
      this.setState({
        modalOpen: true,
        modalMessage: "How are you feeling...?",
        modalBody: '',
        modalStyleClass: ''
      });
    }
  }


  checkHealth(e) {
    e.preventDefault();
    const form = document.forms['vital-form'];
    const bloodSugar = parseInt(form.bloodSugar.value, 0);
    this.checkBloodSugar(bloodSugar, e);
  }

  checkFeeling(e) {
    const feeling = e.currentTarget.dataset.feeling;
    const score = parseInt(e.currentTarget.dataset.feelingscore, 1);
    if(score === 1) { // If user feels okay, just give them some damn food
      this.state.fetchFood(this.state.bloodSugar);
      this.setState({
            modalOpen: false
          })
    } else { // Else if the user feels like shit
      if(this.state.age < 13) { // Check and see if they are under 13 years old
          this.setState({
            modalOpen: true,
            modalMessage: "Alerting guardians",
            modalBody: "I have alerted your guardians. Please remain calm.",
            modalStyleClass: 'relax'
          })
      } else {
        this.setState({
          modalOpen: true,
          modalMessage: "",
          modalBody: "I hope you'll feel better soon. Please have a look at what food you should eat. I will stay in the background and monitor your values. I only want the best for you.",
          modalStyleClass: ''
        })
      }
    }
    this.state.fetchFood(e);
  }

  	render() {

        return (
  	    	<div className="user-values">
              <div className="Values">
                  <ModalView openModal={this.state.modalOpen} modalStyleClass={this.state.modalStyleClass} body={this.state.modalBody} message={this.state.modalMessage} checkFeeling={this.checkFeeling.bind(this)} />
                <form name="vital-form">
                  <label className="formName">Blood sugar:</label>
                  <input type="number" name="bloodSugar" onChange={this.changeBloodSugar.bind(this)} value={this.state.bloodSugar} min="0" max="40" placeholder="Values must be betweeen 0 and 18 mmol/l"/>
                  <button className="Next" type="submit" onClick={this.checkHealth.bind(this)}>Get me food!</button>
                </form>
  		        </div>
    	    </div>
    );
  }
}

export default InputValues;
