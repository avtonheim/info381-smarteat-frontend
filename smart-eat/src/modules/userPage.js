import React, { Component } from 'react';
import 'whatwg-fetch';
import Header from './Header/header.js';
import Categories from './categories';
import EditUserVitals from './editUserVitals';
import sparql from './sparqlQueries/sparql';


class UserPage extends Component {

  constructor(data) {
    super();
    this.state = {
      userID: parseInt(data.match.params.userID, 10),
      bloodSugar: 0,
      name: null,
      categories: null,
      showUserVitals: true
    };
  }

  componentDidMount() {
    fetch(sparql.getUserVitals(this.state.userID))
      .then((res) => res.json())
      .then((body) => {
        var result = body.results.bindings[0];
        this.setState({
          bloodSugar: result.bloodSugarLevel.value,
          name: result.name.value,
          age: result.age.value,
          fatAmount: result.fatAmount.value,
          weight: result.weight.value,
          gender: result.gender.value.toLowerCase()
        });
        // if bloodSugar levels are critical, do some ethical evaluation shit, else fetch food
        this.fetchFood(result.bloodSugarLevel.value);
      })
      .catch( exe => {
        console.error("Fetching failed. Make sure Jena Fuseki server is running on localhost:8080", exe);
      })
  }

   setCategories(categories) {
    this.setState({
      categories: categories.results.bindings
    })
  }

  toggleUserVitals(e) {
    this.setState({
      showUserVitals: !this.state.showUserVitals,
      name: this.state.name,
      age: this.state.age
    })
  }

  getStatusBackgroundColor(e) {
    const bloodSugar = parseInt(this.state.bloodSugar, 0);

    if(bloodSugar <= 2 || bloodSugar >= 22 ) {
      return '#F64341';
    } else if (bloodSugar <= 3 || bloodSugar >= 16) {
      return '#E59B43';  
    }else if(bloodSugar > 8) {
      return '#E9D91F';
    } else {
      return '#50AF47';
    }
  }

  updateBloodSugar(bloodSugar) {
    this.setState({
      bloodSugar
    })
  }

    fetchFood(bloodSugarLevel) {
       if (bloodSugarLevel <= 1) {
            this.getHighCarbAndSugarFoodMoreThan({
              sugar: 10,
              carbLow: 90,
              carbHigh: 100
            });
        } else if (bloodSugarLevel <= 2) {
            this.getHighCarbAndSugarFoodMoreThan({
              sugar: 8,
              carbLow: 70,
              carbHigh: 90
            });
        } else if (bloodSugarLevel <= 3) {
          this.getHighCarbAndSugarFoodMoreThan({
              sugar: 6,
              carbLow: 60,
              carbHigh: 70
            });
        } else if (bloodSugarLevel <= 4) {
            this.getHighCarbAndSugarFoodMoreThan({
              sugar: 4,
              carbLow: 50,
              carbHigh: 60
            });
        } else if (bloodSugarLevel <= 8) {
            this.getHighCarbAndSugarFoodMoreThan({
              sugar: 2,
              carbLow: 15,
              carbHigh: 50
            });
        } else if (bloodSugarLevel <= 16) {
            this.getLowCarbAndSugarFoodLessThan({
              sugar: 0,
              carbLow: 5,
              carbHigh: 15
            });
        } else if (bloodSugarLevel <= 22) {
            this.getLowCarbAndSugarFoodLessThan({
              sugar: 0,
              carbLow: 0,
              carbHigh: 5
            });
        } else if (bloodSugarLevel <= 40) {
            this.getLowCarbAndSugarFoodLessThan({
              sugar: 0,
              carbLow: 0,
              carbHigh: 0
            });
        }
  }

  getHighCarbAndSugarFoodMoreThan(payload) {
    if(payload) {
      fetch(sparql.getHighCarbAndSugarFoodMoreThan(payload.sugar, payload.carbLow, payload.carbHigh))
        .then(res => res.json())
        .then(body => {
          this.setCategories(body);
          return;
        })
    } else {
      console.info("No payload specified for computing food items");
    }
  }

  getLowCarbAndSugarFoodLessThan(payload) {
    if(payload) {
      fetch(sparql.getLowCarbAndSugarFoodLessThan(payload.sugar, payload.carbLow, payload.carbHigh))
        .then(res => res.json())
        .then(body => {
          this.setCategories(body);
        });
    } else {
      console.info("No payload specified for computing food items");
    }
  }


  getLowCarbFood(amountOfCarbs) {
    fetch(sparql.getLowCarbFoodLessThan(amountOfCarbs))
      .then((res) => res.json())
      .then((body) => {
        this.setCategories(body);
      })
      .catch((exe) => {
        console.error("Fetching failed for low carb foods. Check and make sure Jena Fuseki server is running on localhost:8080");
      })
  }

  getHighCarbFood(amountOfCarbs) {
    fetch(sparql.getHighCarbFoodMoreThan(amountOfCarbs))
      .then((res) => res.json())
      .then((body) => {
        this.setCategories(body);
      })
      .catch((exe) => {
        console.error("Fetching failed for high carb foods. Check and make sure Jena Fuseki server is running on localhost:8080");
      })
  }
  //Belongs to status-bar
  //onClick={this.toggleUserVitals.bind(this)}
  render() {

    const userVitals = this.state.showUserVitals ? <EditUserVitals fetchFood={this.fetchFood.bind(this)} updateBloodSugar={this.updateBloodSugar.bind(this)} bloodSugar={this.state.bloodSugar}  name={this.state.name} age={this.state.age} weight={this.state.weight} setCategories={this.setCategories.bind(this)}  /> : '';
    return (
      <div>
        <Header name={this.state.name} age={this.state.age} />
        <div className="status-bar" style={{background: this.getStatusBackgroundColor()}} >
          <ul>
            <li>{this.state.name}, {this.state.age} years old</li>
            <li>Blood sugar level: {this.state.bloodSugar} mmol/l</li>
          </ul>
        </div>
        <div className="container">
       { userVitals }
          <div className="row">
            <Categories categories={this.state.categories} age={parseInt(this.state.age, 0)} />
          </div>
        </div>
      </div>
    );
  }
}

export default UserPage;
