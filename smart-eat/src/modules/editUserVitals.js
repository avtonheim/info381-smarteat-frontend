import React, { Component } from 'react';
import InputValues from './inputValues';

class EditUserVitals extends Component {

	constructor(props) {
		super(props);
		this.state = {
			setCategories: props.setCategories,
			name: props.name,
			age: props.age,
			weight: props.weight,
			bloodSugar: parseInt(props.bloodSugar, 0),
			updateBloodSugar: props.updateBloodSugar,
			fetchFood: props.fetchFood
		}
	}

  bloodSugarChanged(level) {
    if(level < 4 ) {
      this.setState({
        raise: true
      })
    } else if (level > 7) {
      this.setState({
        raise: false
      })
    } else {
      this.setState({
        raise: null
      })
    }
  }


	render() {
		let statusArrow;
	    if(this.state.raise === null) {
	      statusArrow = <p className="bold">Your blood sugar level is okay</p>;
	    } else {
	      statusArrow = this.state.raise ? <p className="bold green">You should raise your blood sugar levels <i className="fa fa-arrow-up green"></i></p> : <p className="bold red">You should lower your blood sugar levels <i className="fa fa-arrow-down red"></i></p>;
	    }
/*
			simulation purposes, from render
			<div className="col-6">
			 <InputValues age={this.state.age} fetchFood={this.state.fetchFood} updateBloodSugar={this.state.updateBloodSugar.bind(this)} fetchCategories={this.state.setCategories.bind(this)} bloodSugarChanged={this.bloodSugarChanged.bind(this)}  />
			</div>
			<div className="row">

		            <div className="col-6">
		              <div className="user-information">
		                  <h2>{this.state.name}</h2>
		                  <small>{this.state.age} years old</small>
		                  <dl>
		                    <dt>Recommended blood sugar level</dt>
		                    <dd>4-8 mmol/l</dd>
		                  </dl>
		              </div>
		            </div>
	          </div>*/
		return (
			<div className="status">
			{ statusArrow }
			</div>
          )
	}

}

export default EditUserVitals;
