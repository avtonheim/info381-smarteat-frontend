import React, { Component } from 'react';
import happyEmoticon from '../emoticons/happy.svg';
import contentEmoticon from '../emoticons/content.svg';
import sadEmoticon from '../emoticons/sad.svg';
import unhappyEmoticon from '../emoticons/unhappy.svg';

class EmojiPanel extends Component {

	constructor(props) {
		super(props);
    this.state = {};
	}


	render() {
			return <div className="emoji-container">
	            <div className="Emoticon" data-feelingScore='1' data-feeling="feeling happy" onClick={this.props.checkFeeling.bind(this)}>
                  <img width="100" src={happyEmoticon} alt="really happy"/>
                  <p>Feeling happy</p>
                </div>
                <div className="Emoticon" data-feelingScore='1' data-feeling="feeling content" onClick={this.props.checkFeeling.bind(this)}>
                  <img width="100" src={contentEmoticon} alt="content"/>
                  <p>Feeling content</p>
                </div>
                <div className="Emoticon" data-feelingScore='0' data-feeling="feeling sad" onClick={this.props.checkFeeling.bind(this)}>
                  <img width="100" src={sadEmoticon} alt="sad"/>
                  <p>Feeling sad</p>
                </div>
                <div className="Emoticon" data-feelingScore='0' data-feeling="feeling unhappy" onClick={this.props.checkFeeling.bind(this)}>
                  <img width="100" src={unhappyEmoticon} alt="unhappy"/>
                  <p>Feeling unhappy</p>
                </div>
             </div>
	}

}

export default EmojiPanel;