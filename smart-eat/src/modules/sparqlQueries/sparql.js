const BASE_URL = 'http://localhost:3030/ds/?query=';

const PREFIXES = [	'prefix xsd: <http://www.w3.org/2001/XMLSchema#>',
					'prefix owl: <http://www.w3.org/2002/07/owl#>',
					'prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>',
					'prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>',
					'prefix smarteat: <http://smarteat.com/ontology/>'].join("");


const USER_QUERY = [
	PREFIXES,
	'	SELECT DISTINCT ?name ?ssn ',
	'WHERE {',
		'?x a smarteat:User;',
	  '  	rdfs:label ?name;',
	 '    	smarteat:ssn ?ssn .',
	'}'
].join("");

const USER_VITALS_QUERY = (ssn) => {
	return `${PREFIXES}
		SELECT DISTINCT ?name ?bloodSugarLevel ?age ?gender ?weight ?fatAmount 
	WHERE { 
		?x a smarteat:User ; 
	    	rdfs:label ?name ; 
	     	smarteat:BloodSugarLevel ?bloodSugarLevel ; 
	     	smarteat:Age ?age ;
         	smarteat:Gender ?gender ;
          	smarteat:Weight ?weight ;
           	smarteat:FatAmount ?fatAmount ;
	     	smarteat:ssn \"${ssn}\" . 
	     }`;
}

const GET_LOW_CARB_FOOD_LESS_THAN = (threshold) => {
return `${PREFIXES} 
		SELECT DISTINCT ?foodName ?carbs ?category ?id
		where { 
		  ?food a smarteat:foodItem ; 
		     	rdfs:label ?foodName ; 
		  		smarteat:Karbohydrat ?carbs ; 
		  		smarteat:id ?id ;
		    	smarteat:category ?category . 
		  filter(?carbs < ${threshold}) 
		} 
		order by desc(?carbs) 
		limit 1000 `;
}

const GET_HIGH_CARB_FOOD_MORE_THAN = (threshold) => {
	return `${PREFIXES} 
			SELECT DISTINCT ?foodName ?carbs ?category ?id  
			where { 
			  ?food a smarteat:foodItem ; 
			     	rdfs:label ?foodName ; 
			  		smarteat:Karbohydrat ?carbs ; 
			  		smarteat:id ?id ;
			    	smarteat:category ?category . 
			  filter(?carbs > ${threshold}) 
			} 
			order by desc(?carbs) 
			limit 1000 `;
}

const GET_HIGH_CARB_AND_SUGAR_FOOD_MORE_THAN = (sugar, carbLow, carbHigh) => {
	return `${PREFIXES} 
			SELECT DISTINCT ?foodName ?carbs ?sugar ?category ?id  
			where { 
			  ?food a smarteat:foodItem ; 
			     	rdfs:label ?foodName ; 
			  		smarteat:Karbohydrat ?carbs ; 
			  		smarteat:id ?id ; 
	      			smarteat:Sukker__tilsatt ?sugar ; 
			    	smarteat:category ?category . 
	  filter(?sugar >= ${sugar})
	  filter(?carbs < ${carbHigh} && ?carbs > ${carbLow}) } `;
}

const GET_LOW_CARB_AND_SUGAR_FOOD_LESS_THAN = (sugar, carbLow, carbHigh) => {
	return `${PREFIXES} 
			SELECT DISTINCT ?foodName ?carbs ?sugar ?category ?id  
			where { 
			  ?food a smarteat:foodItem ; 
			     	rdfs:label ?foodName ; 
			  		smarteat:Karbohydrat ?carbs ; 
			  		smarteat:id ?id ; 
	      			smarteat:Sukker__tilsatt ?sugar ; 
			    	smarteat:category ?category . 
	  filter(?sugar <= ${sugar})
	  filter(?carbs >= ${carbLow} && ?carbs <= ${carbHigh}) } `;
}

const GET_FOOD_FROM_ID = (id) => {
	return `${PREFIXES} 
			SELECT distinct ?foodName ?carbs ?fat ?iron ?kolesterol ?salt ?category 
			WHERE {
			  ?food a smarteat:foodItem ;
			     	rdfs:label ?foodName ;
			  		smarteat:Karbohydrat ?carbs ;
			    	smarteat:Fett ?fat ;
			     	smarteat:Kolesterol ?kolesterol ;
			      	smarteat:Salt ?salt ;
			       	smarteat:Jern ?iron ;
			    	smarteat:category ?category;
				    smarteat:id \"${id}\" .
			}
			limit 1`;
}

var sparql = {

	getUsers: () => {
		return BASE_URL + encodeURIComponent(USER_QUERY);
	},

	getUserVitals: (ssn) => {
		return BASE_URL + encodeURIComponent(USER_VITALS_QUERY(ssn));
	},

	getLowCarbFoodLessThan: (threshold) => {
		return BASE_URL + encodeURIComponent(GET_LOW_CARB_FOOD_LESS_THAN(threshold));
	},

	getHighCarbFoodMoreThan: (threshold) => {
		return BASE_URL + encodeURIComponent(GET_HIGH_CARB_FOOD_MORE_THAN(threshold));
	},

	getFoodFromId: (id) => {
		return BASE_URL + encodeURIComponent(GET_FOOD_FROM_ID(id));
	},

	getHighCarbAndSugarFoodMoreThan: (sugar, carbLow, carbHigh) => {
		return BASE_URL + encodeURIComponent(GET_HIGH_CARB_AND_SUGAR_FOOD_MORE_THAN(sugar, carbLow, carbHigh));
	},

	getLowCarbAndSugarFoodLessThan: (sugar, carbLow, carbHigh) => {
		return BASE_URL + encodeURIComponent(GET_LOW_CARB_AND_SUGAR_FOOD_LESS_THAN(sugar, carbLow, carbHigh));
	}

}


export default sparql;