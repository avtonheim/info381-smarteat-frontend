import React, { Component } from 'react';
import Header from './Header/header';
import sparql from './sparqlQueries/sparql';

class FoodPage extends Component {
	
	constructor(data) {
		super();
		this.state = {
			id: data.match.params.foodId
		};
	}

	componentDidMount() {
		fetch(sparql.getFoodFromId(this.state.id))
		.then(res => res.json())
		.then(body => {
			const result = body.results.bindings[0];
			this.setState({
				name: result.foodName.value,
				carbs: result.carbs.value,
				iron: result.iron.value,
				fat: result.fat.value,
				kolesterol: result.kolesterol.value,
				salt: result.salt.value
			})
		})
		.catch(exe => console.error("There was en error fetching details about food  from foodPage", exe));
	}

	render() {
		return (
			<div className="App">
				<Header />
				<div className="container">
					<h1>{this.state.name}</h1>
					<table className="food-table">
						<thead>
							<tr>
								<th>Næringsinnhold</th>
								<th>Verdi</th>
								<th>Måleenhet</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Karbohydrater</td>
								<td>{this.state.carbs}</td>
								<td>gram</td>
							</tr>
							<tr>
								<td>Jern</td>
								<td>{this.state.iron}</td>
								<td>mg</td>
							</tr>
							<tr>
								<td>Fett</td>
								<td>{this.state.fat}</td>
								<td>gram</td>
							</tr>
							<tr>
								<td>Kolesterol</td>
								<td>{this.state.kolesterol}</td>
								<td>mg</td>
							</tr>
							<tr>
								<td>Salt</td>
								<td>{this.state.salt}</td>
								<td>gram</td>
							</tr>
						</tbody>
					</table>

				</div>
			</div>
		)
	}
}

export default FoodPage;