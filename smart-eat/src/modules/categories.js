import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import logo from '../logo.svg';
import BreadSeeds from '../food-categories/bread-seeds-products-icon.png';
import Diary from '../food-categories/diary-products-icon.png';
import Div from '../food-categories/div-dish-products-icon.png';
import NutsSoda from '../food-categories/div-nuts-soda-products-icon.png';
import Drinks from '../food-categories/drink-products-icon.png';
import Eggs from '../food-categories/egg-products-icon.png';
import Fish from '../food-categories/fish-products-icon.png';
import FruitVegetables from '../food-categories/fruit-vegetables-products-icon.png';
import MeatChicken from '../food-categories/meat-chicken-products-icon.png';

const categories = [
  {
    name: 'Wheat and bread',
    image: BreadSeeds
  },
  {
    name: 'Dairy products',
    image: Diary
  },
  {
    name: 'Diverse meals',
    image: Div
  },
  {
    name: 'Diverse products',
    image: NutsSoda
  },
  {
    name: 'Drinks',
    image: Drinks
  },
  {
    name: 'Egg products',
    image: Eggs
  },
  {
    name: 'Fish and seafood',
    image: Fish
  },
  {
    name: 'Fruit and vegetables',
    image: FruitVegetables
  },
  {
    name: 'Meat',
    image: MeatChicken
  }
]



class FoodCategories extends Component {

  constructor(props) {
    super(props);
    this.state = {
      categories: null
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
    this.createCategories(nextProps.categories, nextProps.age);
  }

  findIcon(name) {
    var x = categories.find(cat => cat.name === name);
    if(!x) {
      return logo;
    } else return x.image;
  }

  createCategories(categories, age) {
    if(categories) {
      var collectedCategories = categories.reduce((list, elem) => {

        var exists = list.find(obj => obj.name === elem.category.value);
        if(exists) {
          exists.count += 1;
          exists.foodItems.push(elem);
        } else {
          list.push({
            name: elem.category.value,
            count: 1,
            icon: this.findIcon(elem.category.value),
            foodItems: [elem]
          });
        }

        return list;
      }, []).filter(cat => cat.count > 0);

      if(age < 18) {
        collectedCategories = collectedCategories.filter(cat => cat.name !== 'Alcohol');
      }

       this.setState({
        categories: collectedCategories
       });
    } else {
      console.log("No categories...");
    }
  }

  render() {
    if(!this.state.categories) {
      return null;
    } else {
       return (
      <div>
      <h1 className="greetMessage">May I suggest that you eat...</h1>
      <div className="row">

        {
        this.state.categories.map((cat, i) => {
          return     <div key={i} className="col-3">
                      <div className="FoodBox">
                        <img src={cat.icon} alt={cat.name}></img>
                        <h2 id={cat.name}>{cat.name} ({cat.count})</h2>
                        <ul>
                        {
                          cat.foodItems.map((item, i) => {
                            return (
                               <Link key={i} to={`/foodItem/${encodeURIComponent(item.id.value)}`}>
                                <li>{item.foodName.value}</li>
                                </Link>
                                )
                          })
                        }
                        </ul>
                      </div>
                    </div>

        })
      }
      </div>
      </div>
    );
    }
  }
}

export default FoodCategories;
