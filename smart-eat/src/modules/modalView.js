import React, { Component } from 'react';
import Modal from 'react-modal';
import EmpojiPanel from './emojiPanel';



class ModalView extends Component {

	constructor(props){
		super(props);
		this.state = {
			openModal: props.openModal,
			checkFeeling: props.checkFeeling
		}
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			openModal: nextProps.openModal
		})
	}

	openModal() {
	    this.setState({
	      openModal: true
	    })
  	}

  	closeModal() {
	    this.setState({
	      openModal: false
	    })
  	}

  	afterOpenModal() {
  	}

	render() {

		let classNames = `overlay content ${this.props.modalStyleClass ? this.props.modalStyleClass : ''}`;

      	if(!this.props.body) {
      		return <Modal
                    isOpen={this.state.openModal}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    closeTimeoutMS={100}
                    contentLabel="Modal"
                    className={classNames}
                    overlayClassName={'content'}
                  >
                    <h1>Information</h1>
                    <p>{this.props.message}</p>
                    <div>
                    	<EmpojiPanel checkFeeling={this.props.checkFeeling} />
                    </div>
                    <button className="modalButton" onClick={this.closeModal.bind(this)}>Close</button>
                  </Modal>
      	} else {

			return <Modal
	                    isOpen={this.state.openModal}
	                    onAfterOpen={this.afterOpenModal}
	                    onRequestClose={this.closeModal}
	                    closeTimeoutMS={100}
	                    contentLabel="Modal"
	                    className={classNames}
	                    overlayClassName={'content'}
	                  >
	                    <h1>Information</h1>
	                    <p>{this.props.message}</p>
	                    <div>
	                    	{ this.props.body }
	                    </div>
	                    <button onClick={this.closeModal.bind(this)}>Close</button>
	                  </Modal>
	            }
	}

}

export default ModalView;