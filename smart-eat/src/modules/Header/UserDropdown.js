import React, { Component } from 'react';
import 'whatwg-fetch';
import sparql from '../sparqlQueries/sparql';


class UserDropdown extends Component {

  constructor() {
    super();
    this.state = {
      users: []
    };
  }

  handleChange(e) {
    window.location.replace(`/user/${e.target.value}`);
  }

  componentDidMount() {
    fetch(sparql.getUsers())
    .then((response) => {
      return response.json();
    })
    .then((body) => {
      this.setState({
        users: body.results.bindings
      })
    });
  }

  render() {
    return (
      <select onChange={this.handleChange.bind(this)} name="userSelect">
        <option value="Choose a user">Select a user</option>
          {
            this.state.users.map((user, i) => <option key={i} value={user.ssn.value}>{user.name.value}</option>)
          }
      </select>
    );
  }
}

export default UserDropdown;
