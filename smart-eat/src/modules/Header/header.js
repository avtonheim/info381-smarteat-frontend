import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import logo from '../../logo.svg';
import '../../grid/simple-grid.min.css';
import './header.css';
import SmartEatPic from '../../kjoleskapdude.png';
import UserDropdown from './UserDropdown';
import InputValues from '../inputValues';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.name,
      isFrontPage: props.isFrontPageOpen
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      name: nextProps.name
    })
  }

  changeName(name) {
    this.setState({name})
  }

  render() {
    const userQuestion = this.state.isFrontPage ? '' : <p>Hello {this.state.name}! I am SmartEat, your intelligent assistant to monitor your diabetes.</p>;
    return (
      <div className="App-Header">
        <div className="Header-content">
          <div className="Simulation">
            <p>Simulation</p>
            <UserDropdown />
            <InputValues />
          </div>
          <div>
            <Link to='/'>
              <h2>SmartEat</h2>
              <img src={SmartEatPic} className="App-logo" alt="logo" />
            </Link>
            { userQuestion }
          </div>
        </div>

      </div>
    );
  }
}

export default Header;
